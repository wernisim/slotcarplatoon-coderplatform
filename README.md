# Distributed Platform for Slotcar Platooning 
Home of the slotcar platooning project based on BeagleBone Blue.
This is a Simulink project for use with distributed BeagleBone platforms.
Contains code to create multiple models destined for different development boards from a single model. 
Separate models are created and ports are replaced with communication blocks.
Start with the Configuration class.
